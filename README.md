# awesome

![awesome](https://gitlab.com/lacat/awesome/raw/master/screen-1.png)

**source themes**:https://github.com/lcpz/awesome-copycats

**推荐:**


​   下拉式终端：Tilda

**依赖:**

​	图标字体：`ttf-font-awesome`



## 插件
### 亮度

**依赖软件**：light

​	查看亮度：light -G

​	增加亮度5单位：light -A 5

​	减少亮度5单位：light -U 5

**快捷键**：

​	增加亮度：`modkey + ; `

​	减少亮度：`modkey + shift + ;`

### 音量

**依赖软件**:amixer

​	增加音量：amixer -D pulse sset Master 5%+

   减少音量：amixer -D pulse sset Master 5%-

**快捷键**：

   增加音量：`modkey + ]`

   减少音量：`modkey + [`

   静音 : `modkey + \`  



